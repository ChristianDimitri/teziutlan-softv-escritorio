Imports System.Data.SqlClient
Public Class FrmSelUsuariosE

    Private Sub FrmSelUsuariosE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSelUsuariosTmpTableAdapter.Connection = CON
        Me.ConSelUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.ConSelUsuariosTmp, eClv_Session, 0)
        CON.Close()
        eBndAtenTelGraf = True
    End Sub

    Private Sub Refrescar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSelUsuariosTmpTableAdapter.Connection = CON
        Me.ConSelUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.ConSelUsuariosTmp, eClv_Session, 1)
        Me.ConSelUsuariosTableAdapter.Connection = CON
        Me.ConSelUsuariosTableAdapter.Fill(Me.DataSetEric2.ConSelUsuarios, eClv_Session)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.NombreListBox.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.InsertarSelUsuariosTableAdapter.Connection = CON
            Me.InsertarSelUsuariosTableAdapter.Fill(Me.DataSetEric2.InsertarSelUsuarios, CInt(Me.NombreListBox.SelectedValue), eClv_Session, 0)
            CON.Close()
            Refrescar()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.NombreListBox.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.InsertarSelUsuariosTableAdapter.Connection = CON
            Me.InsertarSelUsuariosTableAdapter.Fill(Me.DataSetEric2.InsertarSelUsuarios, 0, eClv_Session, 1)
            CON.Close()
            Refrescar()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.NombreListBox1.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.EliminarSelUsuariosTableAdapter.Connection = CON
            Me.EliminarSelUsuariosTableAdapter.Fill(Me.DataSetEric2.EliminarSelUsuarios, CInt(Me.NombreListBox1.SelectedValue), eClv_Session, 0)
            CON.Close()
            Refrescar()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.NombreListBox1.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.EliminarSelUsuariosTableAdapter.Connection = CON
            Me.EliminarSelUsuariosTableAdapter.Fill(Me.DataSetEric2.EliminarSelUsuarios, 0, eClv_Session, 1)
            CON.Close()
            Refrescar()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.NombreListBox1.Items.Count > 0 Then
            FrmImprimirComision.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub
End Class