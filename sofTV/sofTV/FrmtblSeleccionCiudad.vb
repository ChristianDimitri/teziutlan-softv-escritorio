﻿Public Class FrmtblSeleccionCiudad

    Private Sub MUESTRAtblSeleccionCiudad(ByVal Clv_Session As Integer, ByVal Op As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        dTable = BaseII.ConsultaDT("MUESTRAtblSeleccionCiudad")
        If Op = 0 Then dgvIzq.DataSource = dTable
        If Op = 1 Then dgvDer.DataSource = dTable
    End Sub

    Private Sub INSERTAtblSeleccionCiudad(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Ciudad As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_Ciudad)
        BaseII.Inserta("INSERTAtblSeleccionCiudad")
    End Sub

    Private Sub ELIMINAtblSeleccionCiudad(ByVal Clv_Session As Integer, ByVal Op As Integer, ByVal Clv_Ciudad As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_Ciudad)
        BaseII.Inserta("ELIMINAtblSeleccionCiudad")
    End Sub

    Private Sub DameClv_Session()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        eClv_Session = CLng(BaseII.dicoPar("@Clv_Session").ToString)
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        If dgvIzq.SelectedCells(0).Value = 0 Then
            Exit Sub
        End If
        INSERTAtblSeleccionCiudad(eClv_Session, 0, dgvIzq.SelectedCells(0).Value)
        MUESTRAtblSeleccionCiudad(eClv_Session, 0)
        MUESTRAtblSeleccionCiudad(eClv_Session, 1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        INSERTAtblSeleccionCiudad(eClv_Session, 1, 0)
        MUESTRAtblSeleccionCiudad(eClv_Session, 0)
        MUESTRAtblSeleccionCiudad(eClv_Session, 1)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINAtblSeleccionCiudad(eClv_Session, 0, dgvDer.SelectedCells(0).Value)
        MUESTRAtblSeleccionCiudad(eClv_Session, 0)
        MUESTRAtblSeleccionCiudad(eClv_Session, 1)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINAtblSeleccionCiudad(eClv_Session, 1, dgvDer.SelectedCells(0).Value)
        MUESTRAtblSeleccionCiudad(eClv_Session, 0)
        MUESTRAtblSeleccionCiudad(eClv_Session, 1)
    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona una Ciudad.")
            Exit Sub
        End If

        GlobndClv_Ciudad = True

        If dgvDer.RowCount = 1 Then
            GLONOMCiudad = dgvDer.SelectedCells(1).Value
        Else
            GLONOMCiudad = "SELECCIÓN MÚLTIPLE"
        End If

        Me.Close()
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmtblSeleccionCiudad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'DameClv_Session()
        ELIMINAtblSeleccionCiudad(eClv_Session, 1, 0)
        MUESTRAtblSeleccionCiudad(eClv_Session, 0)
        MUESTRAtblSeleccionCiudad(eClv_Session, 1)
    End Sub
End Class