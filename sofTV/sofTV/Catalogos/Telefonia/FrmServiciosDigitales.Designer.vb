<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServiciosDigitales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_ServicioDigitalLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim Precio_UnitarioLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmServiciosDigitales))
        Me.Consulta_ServiciosDigitalesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_ServicioDigitalTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.Precio_UnitarioTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Clv_ServicioDigitalLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Precio_UnitarioLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        CType(Me.Consulta_ServiciosDigitalesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_ServiciosDigitalesBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_ServicioDigitalLabel
        '
        Clv_ServicioDigitalLabel.AutoSize = True
        Clv_ServicioDigitalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_ServicioDigitalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_ServicioDigitalLabel.Location = New System.Drawing.Point(23, 55)
        Clv_ServicioDigitalLabel.Name = "Clv_ServicioDigitalLabel"
        Clv_ServicioDigitalLabel.Size = New System.Drawing.Size(151, 15)
        Clv_ServicioDigitalLabel.TabIndex = 2
        Clv_ServicioDigitalLabel.Text = "Clave Servicio Digital :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(108, 95)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 4
        NombreLabel.Text = "Nombre :"
        '
        'Precio_UnitarioLabel
        '
        Precio_UnitarioLabel.AutoSize = True
        Precio_UnitarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Precio_UnitarioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Precio_UnitarioLabel.Location = New System.Drawing.Point(63, 138)
        Precio_UnitarioLabel.Name = "Precio_UnitarioLabel"
        Precio_UnitarioLabel.Size = New System.Drawing.Size(111, 15)
        Precio_UnitarioLabel.TabIndex = 6
        Precio_UnitarioLabel.Text = "Precio Unitario :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(23, 215)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(252, 15)
        Label2.TabIndex = 8
        Label2.Text = "Servicio correspondiente en el Safari :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(63, 177)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(105, 15)
        Label1.TabIndex = 9
        Label1.Text = "Tipo de Cobro :"
        '
        'Consulta_ServiciosDigitalesBindingNavigator
        '
        Me.Consulta_ServiciosDigitalesBindingNavigator.AddNewItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.CountItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.DeleteItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_ServiciosDigitalesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem})
        Me.Consulta_ServiciosDigitalesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_ServiciosDigitalesBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.Name = "Consulta_ServiciosDigitalesBindingNavigator"
        Me.Consulta_ServiciosDigitalesBindingNavigator.PositionItem = Nothing
        Me.Consulta_ServiciosDigitalesBindingNavigator.Size = New System.Drawing.Size(756, 25)
        Me.Consulta_ServiciosDigitalesBindingNavigator.TabIndex = 4
        Me.Consulta_ServiciosDigitalesBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_ServiciosDigitalesBindingNavigatorSaveItem
        '
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Name = "Consulta_ServiciosDigitalesBindingNavigatorSaveItem"
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Clv_ServicioDigitalTextBox
        '
        Me.Clv_ServicioDigitalTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_ServicioDigitalTextBox.Location = New System.Drawing.Point(187, 52)
        Me.Clv_ServicioDigitalTextBox.Name = "Clv_ServicioDigitalTextBox"
        Me.Clv_ServicioDigitalTextBox.ReadOnly = True
        Me.Clv_ServicioDigitalTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_ServicioDigitalTextBox.TabIndex = 1
        Me.Clv_ServicioDigitalTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BackColor = System.Drawing.Color.White
        Me.NombreTextBox.Location = New System.Drawing.Point(187, 94)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(350, 20)
        Me.NombreTextBox.TabIndex = 1
        '
        'Precio_UnitarioTextBox
        '
        Me.Precio_UnitarioTextBox.BackColor = System.Drawing.Color.White
        Me.Precio_UnitarioTextBox.Location = New System.Drawing.Point(187, 137)
        Me.Precio_UnitarioTextBox.Name = "Precio_UnitarioTextBox"
        Me.Precio_UnitarioTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Precio_UnitarioTextBox.TabIndex = 2
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(608, 332)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 3
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(187, 242)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(77, 21)
        Me.ComboBox1.TabIndex = 7
        Me.ComboBox1.TabStop = False
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(187, 242)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(536, 21)
        Me.ComboBox2.TabIndex = 3
        '
        'ComboBox3
        '
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(187, 242)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(536, 21)
        Me.ComboBox3.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RadioButton2)
        Me.Panel1.Controls.Add(Me.RadioButton1)
        Me.Panel1.Location = New System.Drawing.Point(187, 163)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(247, 49)
        Me.Panel1.TabIndex = 10
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.Blue
        Me.RadioButton2.Location = New System.Drawing.Point(130, 14)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(94, 19)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.Text = "&Por Evento"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.Blue
        Me.RadioButton1.Location = New System.Drawing.Point(23, 14)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(80, 19)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "&Mensual"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'FrmServiciosDigitales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(756, 380)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Clv_ServicioDigitalLabel)
        Me.Controls.Add(Me.Clv_ServicioDigitalTextBox)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.NombreTextBox)
        Me.Controls.Add(Precio_UnitarioLabel)
        Me.Controls.Add(Me.Precio_UnitarioTextBox)
        Me.Controls.Add(Me.Consulta_ServiciosDigitalesBindingNavigator)
        Me.Controls.Add(Me.ComboBox3)
        Me.MaximizeBox = False
        Me.Name = "FrmServiciosDigitales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicios Digitales"
        CType(Me.Consulta_ServiciosDigitalesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_ServiciosDigitalesBindingNavigator.ResumeLayout(False)
        Me.Consulta_ServiciosDigitalesBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Consulta_ServiciosDigitalesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_ServiciosDigitalesBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_ServicioDigitalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Precio_UnitarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
End Class
