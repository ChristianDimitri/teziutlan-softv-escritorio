Imports System.Data.SqlClient
Public Class FrmGeneraCarteras

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GloBndGenCartera = False
        GloBndGenCartera_Pregunta = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GENERAMICARTERA()
        Me.Close()
    End Sub

    Private Sub GENERAMICARTERA()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If GloClv_TipSer <> 3 Then
                Me.Genera_CarteraTableAdapter.Connection = CON
                Me.Genera_CarteraTableAdapter.Fill(Me.DataSetEDGAR.Genera_Cartera, glosessioncar, GloClv_TipSer, 0, GloUsuario, GLOClv_Cartera)
            Else
                Me.Genera_CarteraDIGTableAdapter.Connection = CON
                Me.Genera_CarteraDIGTableAdapter.Fill(Me.DataSetEdgarRev2.Genera_CarteraDIG, glosessioncar, GloClv_TipSer, 0, GloUsuario, GLOClv_Cartera)
            End If
            GloBndGenCartera_Pregunta = True
            GloBndGenCartera = True
            MsgBox("Se Genero la Cartera con Exito ", MsgBoxStyle.Information)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmGeneraCarteras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

End Class