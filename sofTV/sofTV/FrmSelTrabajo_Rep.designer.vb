<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelTrabajo_Rep
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.CMBLABEL = New System.Windows.Forms.Label
        Me.CMB2Label1 = New System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.MuestraSeleccionaTrabajoConsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.MuestraSeleccionaTrabajotmpConsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_Trabajo_tmpConsultaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_Trabajo_tmpConsultaTableAdapter
        Me.MuestraSeleccionaTrabajotmpConsultaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_TrabajoTmpNuevoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_TrabajoTmpNuevoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TrabajoTmpNuevoTableAdapter
        Me.Insertauno_Seleccion_TrabajoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_TrabajoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_TrabajoTableAdapter
        Me.Insertauno_Seleccion_Trabajo_tmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Insertauno_Seleccion_Trabajo_tmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_Trabajo_tmpTableAdapter
        Me.InsertaTOSeleccion_trabajoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSeleccion_trabajoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSeleccion_trabajoTableAdapter
        Me.InsertaTOSeleccion_trabajo_tmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsertaTOSeleccion_trabajo_tmpTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSeleccion_trabajo_tmpTableAdapter
        Me.MuestraSelecciona_TrabajoConsultaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TrabajoConsultaTableAdapter
        Me.CMBLabel7 = New System.Windows.Forms.Label
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.Borra_Seleccion_trabajoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Seleccion_trabajoTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Seleccion_trabajoTableAdapter
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
        CType(Me.MuestraSeleccionaTrabajoConsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaTrabajotmpConsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaTrabajotmpConsultaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelecciona_TrabajoTmpNuevoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_TrabajoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Insertauno_Seleccion_Trabajo_tmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSeleccion_trabajoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsertaTOSeleccion_trabajo_tmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Seleccion_trabajoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLABEL
        '
        Me.CMBLABEL.AutoSize = True
        Me.CMBLABEL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLABEL.ForeColor = System.Drawing.Color.Brown
        Me.CMBLABEL.Location = New System.Drawing.Point(409, 75)
        Me.CMBLABEL.Name = "CMBLABEL"
        Me.CMBLABEL.Size = New System.Drawing.Size(183, 16)
        Me.CMBLABEL.TabIndex = 51
        Me.CMBLABEL.Text = "Trabajos Seleccionados:"
        '
        'CMB2Label1
        '
        Me.CMB2Label1.AutoSize = True
        Me.CMB2Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMB2Label1.ForeColor = System.Drawing.Color.Brown
        Me.CMB2Label1.Location = New System.Drawing.Point(30, 75)
        Me.CMB2Label1.Name = "CMB2Label1"
        Me.CMB2Label1.Size = New System.Drawing.Size(198, 16)
        Me.CMB2Label1.TabIndex = 50
        Me.CMB2Label1.Text = "Trabajos para Seleccionar:"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(412, 430)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 47
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(156, 430)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 46
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(316, 266)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 45
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(316, 237)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 44
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(316, 169)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 43
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(316, 140)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 42
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'MuestraSeleccionaTrabajoConsultaBindingSource
        '
        Me.MuestraSeleccionaTrabajoConsultaBindingSource.DataMember = "MuestraSelecciona_TrabajoConsulta"
        Me.MuestraSeleccionaTrabajoConsultaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MuestraSeleccionaTrabajotmpConsultaBindingSource
        Me.ListBox1.DisplayMember = "Descripcion"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(30, 94)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(262, 290)
        Me.ListBox1.TabIndex = 48
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_trabajos"
        '
        'MuestraSeleccionaTrabajotmpConsultaBindingSource
        '
        Me.MuestraSeleccionaTrabajotmpConsultaBindingSource.DataMember = "MuestraSelecciona_Trabajo_tmpConsulta"
        Me.MuestraSeleccionaTrabajotmpConsultaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSelecciona_Trabajo_tmpConsultaTableAdapter
        '
        Me.MuestraSelecciona_Trabajo_tmpConsultaTableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccionaTrabajotmpConsultaBindingSource1
        '
        Me.MuestraSeleccionaTrabajotmpConsultaBindingSource1.DataMember = "MuestraSelecciona_Trabajo_tmpConsulta"
        Me.MuestraSeleccionaTrabajotmpConsultaBindingSource1.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSelecciona_TrabajoTmpNuevoBindingSource
        '
        Me.MuestraSelecciona_TrabajoTmpNuevoBindingSource.DataMember = "MuestraSelecciona_TrabajoTmpNuevo"
        Me.MuestraSelecciona_TrabajoTmpNuevoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'MuestraSelecciona_TrabajoTmpNuevoTableAdapter
        '
        Me.MuestraSelecciona_TrabajoTmpNuevoTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_TrabajoBindingSource
        '
        Me.Insertauno_Seleccion_TrabajoBindingSource.DataMember = "Insertauno_Seleccion_Trabajo"
        Me.Insertauno_Seleccion_TrabajoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_TrabajoTableAdapter
        '
        Me.Insertauno_Seleccion_TrabajoTableAdapter.ClearBeforeFill = True
        '
        'Insertauno_Seleccion_Trabajo_tmpBindingSource
        '
        Me.Insertauno_Seleccion_Trabajo_tmpBindingSource.DataMember = "Insertauno_Seleccion_Trabajo_tmp"
        Me.Insertauno_Seleccion_Trabajo_tmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Insertauno_Seleccion_Trabajo_tmpTableAdapter
        '
        Me.Insertauno_Seleccion_Trabajo_tmpTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSeleccion_trabajoBindingSource
        '
        Me.InsertaTOSeleccion_trabajoBindingSource.DataMember = "InsertaTOSeleccion_trabajo"
        Me.InsertaTOSeleccion_trabajoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSeleccion_trabajoTableAdapter
        '
        Me.InsertaTOSeleccion_trabajoTableAdapter.ClearBeforeFill = True
        '
        'InsertaTOSeleccion_trabajo_tmpBindingSource
        '
        Me.InsertaTOSeleccion_trabajo_tmpBindingSource.DataMember = "InsertaTOSeleccion_trabajo_tmp"
        Me.InsertaTOSeleccion_trabajo_tmpBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'InsertaTOSeleccion_trabajo_tmpTableAdapter
        '
        Me.InsertaTOSeleccion_trabajo_tmpTableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_TrabajoConsultaTableAdapter
        '
        Me.MuestraSelecciona_TrabajoConsultaTableAdapter.ClearBeforeFill = True
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel7.Location = New System.Drawing.Point(12, 9)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(154, 18)
        Me.CMBLabel7.TabIndex = 53
        Me.CMBLabel7.Text = "Tipo de Servicios  :"
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBox4.DisplayMember = "Concepto"
        Me.ComboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.ForeColor = System.Drawing.Color.Red
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(12, 32)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(226, 24)
        Me.ComboBox4.TabIndex = 52
        Me.ComboBox4.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Borra_Seleccion_trabajoBindingSource
        '
        Me.Borra_Seleccion_trabajoBindingSource.DataMember = "Borra_Seleccion_trabajo"
        Me.Borra_Seleccion_trabajoBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Borra_Seleccion_trabajoTableAdapter
        '
        Me.Borra_Seleccion_trabajoTableAdapter.ClearBeforeFill = True
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSeleccionaTrabajoConsultaBindingSource
        Me.ListBox2.DisplayMember = "Descripcion"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(412, 94)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(256, 303)
        Me.ListBox2.TabIndex = 49
        Me.ListBox2.TabStop = False
        Me.ListBox2.ValueMember = "clv_trabajos"
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'FrmSelTrabajo_Rep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 490)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Controls.Add(Me.ComboBox4)
        Me.Controls.Add(Me.CMBLABEL)
        Me.Controls.Add(Me.CMB2Label1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "FrmSelTrabajo_Rep"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccion Trabajos Técnico Reportes"
        CType(Me.MuestraSeleccionaTrabajoConsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaTrabajotmpConsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaTrabajotmpConsultaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelecciona_TrabajoTmpNuevoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_TrabajoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Insertauno_Seleccion_Trabajo_tmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSeleccion_trabajoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsertaTOSeleccion_trabajo_tmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Seleccion_trabajoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLABEL As System.Windows.Forms.Label
    Friend WithEvents CMB2Label1 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents MuestraSeleccionaTrabajotmpConsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents MuestraSelecciona_Trabajo_tmpConsultaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_Trabajo_tmpConsultaTableAdapter
    Friend WithEvents MuestraSeleccionaTrabajotmpConsultaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_TrabajoTmpNuevoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_TrabajoTmpNuevoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TrabajoTmpNuevoTableAdapter
    Friend WithEvents Insertauno_Seleccion_TrabajoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_TrabajoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_TrabajoTableAdapter
    Friend WithEvents Insertauno_Seleccion_Trabajo_tmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Insertauno_Seleccion_Trabajo_tmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Insertauno_Seleccion_Trabajo_tmpTableAdapter
    Friend WithEvents InsertaTOSeleccion_trabajoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSeleccion_trabajoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSeleccion_trabajoTableAdapter
    Friend WithEvents InsertaTOSeleccion_trabajo_tmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsertaTOSeleccion_trabajo_tmpTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.InsertaTOSeleccion_trabajo_tmpTableAdapter
    Friend WithEvents MuestraSeleccionaTrabajoConsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_TrabajoConsultaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.MuestraSelecciona_TrabajoConsultaTableAdapter
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents Borra_Seleccion_trabajoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Seleccion_trabajoTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Borra_Seleccion_trabajoTableAdapter
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
End Class
