﻿
Imports System.Text
Imports System.Data.SqlClient

Public Class FrmDetDecodificadores


    Private Sub ConTblDecodificadores()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConTblDecodificadores")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbDecodificador.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraTipoClientes()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraTipoClientes 0,0")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbTipoCliente.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraServiciosEric()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraServiciosEric 3,0,0")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbServicio.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConDetTblDecodificadores(ByVal Id As Integer, ByVal Clv_TipoCliente As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConDetTblDecodificadores " & CStr(Id) & ", " & CStr(Clv_TipoCliente))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgvDet.DataSource = dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub NueDetTblDecoficiadores(ByVal Id As Integer, ByVal Clv_TipoCliente As Integer, ByVal Clv_Servicio As Integer, ByVal PrecioPrincipal As Decimal, ByVal PrecioAdicional As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetTblDecoficiadores", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0


        Dim par0 As New SqlParameter("@Id", SqlDbType.Int)
        par0.Direction = ParameterDirection.Input
        par0.Value = Id
        comando.Parameters.Add(par0)

        Dim par1 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_TipoCliente
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Servicio
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@PrecioPrincipal", SqlDbType.Decimal)
        par3.Direction = ParameterDirection.Input
        par3.Value = PrecioPrincipal
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@PrecioAdicional", SqlDbType.Decimal)
        par4.Direction = ParameterDirection.Input
        par4.Value = PrecioAdicional
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorDetTblDecoficiadores(ByVal Detalle As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDetTblDecoficiadores", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Detalle", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Detalle
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = ""
            eRes = par2.Value
            eMsj = par3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub FrmDetDecodificadores_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        ConTblDecodificadores()
        MuestraTipoClientes()
        MuestraServiciosEric()
        ConDetTblDecodificadores(cbDecodificador.SelectedValue, cbTipoCliente.SelectedValue)
    End Sub

    Private Sub txtAgregarDec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAgregarDec.Click
        If txtPrecioPrincipal.Text.Length = 0 Then
            MsgBox("Captura Precio de Principal.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If txtPrecioAdicional.Text.Length = 0 Then
            MsgBox("Captura Precio de Adicional.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueDetTblDecoficiadores(cbDecodificador.SelectedValue, cbTipoCliente.SelectedValue, cbServicio.SelectedValue, txtPrecioPrincipal.Text, txtPrecioAdicional.Text)
        ConDetTblDecodificadores(cbDecodificador.SelectedValue, cbTipoCliente.SelectedValue)
    End Sub

    Private Sub txtEliminarDec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEliminarDec.Click
        If dgvDet.Rows.Count = 0 Then
            MsgBox("Selecciona un registro a eliminar.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorDetTblDecoficiadores(dgvDet.SelectedCells.Item(0).Value)
        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        ConDetTblDecodificadores(cbDecodificador.SelectedValue, cbTipoCliente.SelectedValue)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.close()
    End Sub

    Private Sub cbDecodificador_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDecodificador.SelectedIndexChanged
        ConDetTblDecodificadores(cbDecodificador.SelectedValue, cbTipoCliente.SelectedValue)
    End Sub

    Private Sub cbTipoCliente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTipoCliente.SelectedIndexChanged
        ConDetTblDecodificadores(cbDecodificador.SelectedValue, cbTipoCliente.SelectedValue)
    End Sub

    Private Sub txtPrecioPrincipal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrecioPrincipal.KeyPress
        e.KeyChar = Chr((ValidaKey(txtPrecioPrincipal, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub txtPrecioAdicional_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrecioAdicional.KeyPress
        e.KeyChar = Chr((ValidaKey(txtPrecioAdicional, Asc(LCase(e.KeyChar)), "L")))
    End Sub
End Class