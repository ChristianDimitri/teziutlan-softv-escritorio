Imports System.Data.SqlClient
Public Class BrwSelContrato55
    Dim Table As New DataTable
    Dim Op As Integer = 3
    Dim contrato As Long = 0
    Dim nombre As String = ""
    Dim calle As String = ""
    Dim numero As String = ""
    Dim ciudad As String = ""
    Dim permiso As Boolean = False

    Private Sub BrwSelContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'colorea(Me, Me.Name)
        'Me.DameClientesActivosTableAdapter.Connection = CON
        'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", "", "", "", 3)
        'CON.Close()
        colorea(Me, Me.Name)
        Op = 3
        contrato = 0
        nombre = ""
        Dim calle As String = ""
        Dim numero As String = ""
        Dim ciudad As String = ""
        llenaGrid()
        permiso = True
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.CONTRATOLabel1.Text.Length > 0 Then
            eGloContrato = Me.CONTRATOLabel1.Text
            eGloSubContrato = Me.CONTRATOLabel1.Text
            eContrato = Me.CONTRATOLabel1.Text
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            eGloContrato = 0
            eGloSubContrato = 0
            eContrato = 0
        End If
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        eGloContrato = 0
        eGloSubContrato = 0
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If Asc(e.KeyChar) = 13 Then
        '    If IsNumeric(Me.TextBox1.Text) = True Then
        '        Me.DameClientesActivosTableAdapter.Connection = CON
        '        Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
        '    End If
        'End If
        'CON.Close()
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                Op = 0
                contrato = Me.TextBox1.Text
                nombre = ""
                Dim calle As String = ""
                Dim numero As String = ""
                Dim ciudad As String = ""
                llenaGrid()
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If IsNumeric(Me.TextBox1.Text) = True Then
        '    Me.DameClientesActivosTableAdapter.Connection = CON
        '    Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
        'End If
        'CON.Close()
        If IsNumeric(Me.TextBox1.Text) = True Then
            Op = 0
            contrato = Me.TextBox1.Text
            nombre = ""
            Dim calle As String = ""
            Dim numero As String = ""
            Dim ciudad As String = ""
            llenaGrid()
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If Asc(e.KeyChar) = 13 Then
        '    If Me.TextBox2.Text.Length > 0 Then
        '        Me.DameClientesActivosTableAdapter.Connection = CON
        '        Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, Me.TextBox2.Text, "", "", "", 1)
        '    End If
        'End If
        'CON.Close()
        If Asc(e.KeyChar) Then
            If Me.TextBox2.Text.Length > 0 Then
                Op = 1
                contrato = 0
                nombre = Me.TextBox2.Text
                Dim calle As String = ""
                Dim numero As String = ""
                Dim ciudad As String = ""
                llenaGrid()
            End If
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If Me.TextBox2.Text.Length > 0 Then
        '    Me.DameClientesActivosTableAdapter.Connection = CON
        '    Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, Me.TextBox2.Text, "", "", "", 1)
        'End If
        'CON.Close()
        If Me.TextBox2.Text.Length > 0 Then
            Op = 1
            contrato = 0
            nombre = Me.TextBox2.Text
            Dim calle As String = ""
            Dim numero As String = ""
            Dim ciudad As String = ""
            llenaGrid()
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If Asc(e.KeyChar) = 13 Then
        '    If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        '    If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        '    If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        '    Me.DameClientesActivosTableAdapter.Connection = CON
        '    Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        'End If
        'CON.Close()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Op = 2
            contrato = 0
            nombre = ""
            Dim calle As String = Me.TextBox3.Text
            Dim numero As String = Me.TextBox4.Text
            Dim ciudad As String = Me.TextBox5.Text
            llenaGrid()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        'If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        'If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        'Me.DameClientesActivosTableAdapter.Connection = CON
        'Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        'CON.Close(
        If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        Op = 2
        contrato = 0
        nombre = ""
        Dim calle As String = Me.TextBox3.Text
        Dim numero As String = Me.TextBox4.Text
        Dim ciudad As String = Me.TextBox5.Text
        llenaGrid()
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If Asc(e.KeyChar) = 13 Then
        '    If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        '    If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        '    If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        '    Me.DameClientesActivosTableAdapter.Connection = CON
        '    Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        'End If
        'CON.Close()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Op = 2
            contrato = 0
            nombre = ""
            Dim calle As String = Me.TextBox3.Text
            Dim numero As String = Me.TextBox4.Text
            Dim ciudad As String = Me.TextBox5.Text
            llenaGrid()
        End If
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'If Asc(e.KeyChar) = 13 Then
        '    If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
        '    If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
        '    If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
        '    Me.DameClientesActivosTableAdapter.Connection = CON
        '    Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, 0, "", Me.TextBox3.Text, Me.TextBox4.Text, Me.TextBox5.Text, 2)
        'End If
        'CON.Close()
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length = 0 Then Me.TextBox3.Text = ""
            If Me.TextBox4.Text.Length = 0 Then Me.TextBox4.Text = ""
            If Me.TextBox5.Text.Length = 0 Then Me.TextBox5.Text = ""
            Op = 2
            contrato = 0
            nombre = ""
            Dim calle As String = Me.TextBox3.Text
            Dim numero As String = Me.TextBox4.Text
            Dim ciudad As String = Me.TextBox5.Text
            llenaGrid()
        End If
    End Sub
    Private Sub llenaGrid()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, contrato)
        BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, nombre)
        BaseII.CreateMyParameter("@CALLE", SqlDbType.VarChar, calle)
        BaseII.CreateMyParameter("@NUMERO", SqlDbType.VarChar, numero)
        BaseII.CreateMyParameter("@CIUDAD", SqlDbType.VarChar, ciudad)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        Table = BaseII.ConsultaDT("DameClientesActivos5")
        DameClientesActivosDataGridView.DataSource = Table
        If (DameClientesActivosDataGridView.RowCount > 0) Then
            'clv_TipSer = Me.GridServiciosDelCliente.SelectedCells(2).Value.ToString
            Me.CONTRATOLabel1.Text = DameClientesActivosDataGridView.SelectedCells(0).Value.ToString
            Me.NOMBRELabel1.Text = DameClientesActivosDataGridView.SelectedCells(1).Value.ToString
            Me.CALLELabel1.Text = DameClientesActivosDataGridView.SelectedCells(2).Value.ToString
            Me.NUMEROLabel1.Text = DameClientesActivosDataGridView.SelectedCells(4).Value.ToString
            Me.COLONIALabel1.Text = DameClientesActivosDataGridView.SelectedCells(3).Value.ToString
            Me.CIUDADLabel1.Text = DameClientesActivosDataGridView.SelectedCells(5).Value.ToString
            If DameClientesActivosDataGridView.SelectedCells(6).Value = True Then
                SOLOINTERNETCheckBox.Checked = True
            Else
                SOLOINTERNETCheckBox.Checked = False
            End If
            If DameClientesActivosDataGridView.SelectedCells(7).Value = True Then
                ESHOTELCheckBox.Checked = True
            Else
                ESHOTELCheckBox.Checked = False
            End If
        End If
    End Sub

    Private Sub DameClientesActivosDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DameClientesActivosDataGridView.CellContentClick
        If (DameClientesActivosDataGridView.RowCount > 0) Then
            'clv_TipSer = Me.GridServiciosDelCliente.SelectedCells(2).Value.ToString
            Me.CONTRATOLabel1.Text = DameClientesActivosDataGridView.SelectedCells(0).Value.ToString
            Me.NOMBRELabel1.Text = DameClientesActivosDataGridView.SelectedCells(1).Value.ToString
            Me.CALLELabel1.Text = DameClientesActivosDataGridView.SelectedCells(2).Value.ToString
            Me.NUMEROLabel1.Text = DameClientesActivosDataGridView.SelectedCells(4).Value.ToString
            Me.COLONIALabel1.Text = DameClientesActivosDataGridView.SelectedCells(3).Value.ToString
            Me.CIUDADLabel1.Text = DameClientesActivosDataGridView.SelectedCells(5).Value.ToString
            If DameClientesActivosDataGridView.SelectedCells(6).Value = True Then
                SOLOINTERNETCheckBox.Checked = True
            Else
                SOLOINTERNETCheckBox.Checked = False
            End If
            If DameClientesActivosDataGridView.SelectedCells(7).Value = True Then
                ESHOTELCheckBox.Checked = True
            Else
                ESHOTELCheckBox.Checked = False
            End If
        End If
    End Sub

    Private Sub DameClientesActivosDataGridView_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DameClientesActivosDataGridView.SelectionChanged
        Try
            If (DameClientesActivosDataGridView.RowCount > 0) And permiso = True Then
                'clv_TipSer = Me.GridServiciosDelCliente.SelectedCells(2).Value.ToString
                Me.CONTRATOLabel1.Text = DameClientesActivosDataGridView.SelectedCells(0).Value.ToString
                Me.NOMBRELabel1.Text = DameClientesActivosDataGridView.SelectedCells(1).Value.ToString
                Me.CALLELabel1.Text = DameClientesActivosDataGridView.SelectedCells(2).Value.ToString
                Me.NUMEROLabel1.Text = DameClientesActivosDataGridView.SelectedCells(4).Value.ToString
                Me.COLONIALabel1.Text = DameClientesActivosDataGridView.SelectedCells(3).Value.ToString
                Me.CIUDADLabel1.Text = DameClientesActivosDataGridView.SelectedCells(5).Value.ToString
                If DameClientesActivosDataGridView.SelectedCells(6).Value = True Then
                    SOLOINTERNETCheckBox.Checked = True
                Else
                    SOLOINTERNETCheckBox.Checked = False
                End If
                If DameClientesActivosDataGridView.SelectedCells(7).Value = True Then
                    ESHOTELCheckBox.Checked = True
                Else
                    ESHOTELCheckBox.Checked = False
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class