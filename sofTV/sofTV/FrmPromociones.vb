﻿Imports System.Data.SqlClient
Imports System.Text
Public Class FrmPromociones

    Dim Clv_TipoCliente As Integer = 0
    Dim Clave As Integer = 0



    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraServiciosEric " & CStr(Clv_TipSer) & " , " & CStr(Clv_Servicio) & " , " & CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cServicio.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraSoloTarifadosOp(ByVal Op As Integer, ByVal Clave As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraSoloTarifadosOp " & CStr(Op) & " , " & CStr(Clave))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbConcepto.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraTipoClientes(ByVal Clv_TipoCliente As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraTipoClientes " & CStr(Clv_TipoCliente) & " , " & CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cbTipoCobro.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub DAMEPRECIOSERV(ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DAMEPRECIOSERV", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Servicio
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                tbPrecio.Text = reader(0).ToString
            End While

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConPromocion(ByVal Op As Integer, ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Nombre As String, ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConPromocion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Op", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Op
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Servicio
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Nombre", SqlDbType.VarChar, 250)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Nombre
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Id", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Id
        comando.Parameters.Add(parametro5)

        Try

            conexion.Open()

            reader = comando.ExecuteReader

            While (reader.Read())
                Id = reader(0).ToString()
                tbNombre.Text = reader(1).ToString
                tbPagos.Text = reader(2).ToString
                tbPrecio.Text = reader(3).ToString
                dtFechaIni.Value = reader(4).ToString()
                dtFechaFin.Value = reader(5).ToString()
                eClv_Servicio = reader(6).ToString
                CheckBox1.Checked = reader(7).ToString()
                Clave = reader(8).ToString()
                checkPregunta.Checked = reader(9).ToString()
                Clv_TipoCliente = reader(10).ToString()

            End While

            MuestraTipoClientes(Clv_TipoCliente, 1)
            MuestraServiciosEric(eClv_TipSer, eClv_Servicio, 5)
            MuestraSoloTarifadosOp(1, Clave)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NuePromocion(ByVal Nombre As String, ByVal Pagos As Integer, ByVal Precio As Decimal, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Clv_Servicio As Integer, ByVal Vigente As Boolean, ByVal Clave As Integer, ByVal Pregunta As Boolean, ByVal Clv_TipoCliente As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NuePromocion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Nombre", SqlDbType.VarChar, 150)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Nombre
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("Pagos", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Pagos
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Precio", SqlDbType.Decimal)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Precio
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@FechaIni", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = FechaIni
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@FechaFin", SqlDbType.DateTime)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = FechaFin
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Clv_Servicio
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Vigente", SqlDbType.Bit)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = Vigente
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Clave", SqlDbType.Int)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = Clave
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Pregunta", SqlDbType.Bit)
        parametro9.Direction = ParameterDirection.Input
        parametro9.Value = Pregunta
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        parametro10.Direction = ParameterDirection.Input
        parametro10.Value = Clv_TipoCliente
        comando.Parameters.Add(parametro10)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

            MsgBox(mensaje5, MsgBoxStyle.Information)
            eBndPromocion = True
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ModPromocion(ByVal Id As Integer, ByVal Nombre As String, ByVal Pagos As Integer, ByVal Precio As Decimal, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Clv_Servicio As Integer, ByVal Vigente As Boolean, ByVal Clave As Integer, ByVal Pregunta As Boolean, ByVal Clv_TipoCliente As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModPromocion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Id", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Id
        comando.Parameters.Add(par)

        Dim parametro As New SqlParameter("@Nombre", SqlDbType.VarChar, 150)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Nombre
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("Pagos", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Pagos
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Precio", SqlDbType.Decimal)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Precio
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@FechaIni", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = FechaIni
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@FechaFin", SqlDbType.DateTime)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = FechaFin
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Clv_Servicio
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Vigente", SqlDbType.Bit)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = Vigente
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Clave", SqlDbType.Int)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = Clave
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Pregunta", SqlDbType.Bit)
        parametro9.Direction = ParameterDirection.Input
        parametro9.Value = Pregunta
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Clv_TipoCliente", SqlDbType.Int)
        parametro10.Direction = ParameterDirection.Input
        parametro10.Value = Pregunta
        comando.Parameters.Add(parametro10)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

            eRes = 0
            eMsj = String.Empty
            eRes = par2.Value
            eMsj = par3.Value

            If eRes = 0 Then
                MsgBox(mensaje5, MsgBoxStyle.Information)
                eBndPromocion = True
            Else
                MsgBox(eMsj, MsgBoxStyle.Exclamation)
            End If

            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorPromocion(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorPromocion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Id", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Id
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            conexion.Open()

            comando.ExecuteNonQuery()

            eRes = 0
            eMsj = String.Empty
            eRes = par2.Value
            eMsj = par3.Value

            If eRes = 0 Then
                MsgBox(mensaje6, MsgBoxStyle.Information)
                eBndPromocion = True
            Else
                MsgBox(eMsj, MsgBoxStyle.Exclamation)
            End If

            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub TSBGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBGuardar.Click

        If tbNombre.Text.Length = 0 Then
            MsgBox("Captura un Nombre.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If IsNumeric(cServicio.SelectedValue) = False Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If IsNumeric(tbPrecio.Text) = False Then
            MsgBox("Teclea una tarifa válida.", MsgBoxStyle.Exclamation)
        End If

        If IsNumeric(tbPagos.Text) = False Then
            MsgBox("Captura el número de Pagos", MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        If eOpcion = "N" Then
            NuePromocion(tbNombre.Text, tbPagos.Text, tbPrecio.Text, dtFechaIni.Value, dtFechaFin.Value, cServicio.SelectedValue, CheckBox1.Checked, cbConcepto.SelectedValue, checkPregunta.Checked, cbTipoCobro.SelectedValue)
        ElseIf eOpcion = "M" Then
            ModPromocion(eIdPromocion, tbNombre.Text, tbPagos.Text, tbPrecio.Text, dtFechaIni.Value, dtFechaFin.Value, cServicio.SelectedValue, CheckBox1.Checked, cbConcepto.SelectedValue, checkPregunta.Checked, cbTipoCobro.SelectedValue)
        End If

    End Sub

    Private Sub TSBEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBEliminar.Click
        BorPromocion(eIdPromocion)
    End Sub

    Private Sub FrmPromociones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If eOpcion = "N" Then
            MuestraServiciosEric(eClv_TipSer, 0, 4)
            MuestraTipoClientes(0, 0)
            MuestraSoloTarifadosOp(0, 0)
            TSBEliminar.Enabled = False
            dtFechaIni.Value = Today
            dtFechaIni.MinDate = Today
            dtFechaFin.Value = Today
        ElseIf eOpcion = "C" Then
            ConPromocion(3, 0, 0, String.Empty, eIdPromocion)
            bnPromocion.Enabled = False
        ElseIf eOpcion = "M" Then
            ConPromocion(3, 0, 0, String.Empty, eIdPromocion)

        End If
        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub cServicio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cServicio.SelectedIndexChanged
        'DAMEPRECIOSERV(cServicio.SelectedValue)
    End Sub

    Private Sub dtFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtFechaIni.ValueChanged
        dtFechaFin.MinDate = dtFechaIni.Value
    End Sub

    Private Sub dtFechaFin_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtFechaFin.ValueChanged
        dtFechaIni.MaxDate = dtFechaFin.Value
    End Sub

    Private Sub tbPagos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbPagos.KeyPress
        e.KeyChar = ChrW(ValidaKey(tbPagos, Asc(e.KeyChar), "N"))
    End Sub

    Private Sub tbNombre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbNombre.KeyPress
        e.KeyChar = ChrW(ValidaKey(tbNombre, Asc(e.KeyChar), "S"))
    End Sub

    Private Sub bSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bSalir.Click
        Me.Close()
    End Sub

    Private Sub cbConcepto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbConcepto.SelectedIndexChanged
        If cbConcepto.SelectedValue = 1 Then
            Me.checkPregunta.Enabled = True
        Else
            Me.checkPregunta.Checked = False
            Me.checkPregunta.Enabled = False
        End If
    End Sub

    Private Sub cbTipoCobro_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'MsgBox(cbTipoCobro.SelectedValue)
    End Sub
End Class