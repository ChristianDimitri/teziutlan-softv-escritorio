﻿Public Class FrmRepClientesClaveTecnicaFtth

    Private Sub MUESTRASectorReporte()
        BaseII.limpiaParametros()
        cbSector.DataSource = BaseII.ConsultaDT("MUESTRAHubReporte")
    End Sub

    Private Sub MUESTRAPosteReporte()
        BaseII.limpiaParametros()
        cbPoste.DataSource = BaseII.ConsultaDT("MUESTRAOLTReporte")
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If cbSector.Text.Length = 0 Then
            MessageBox.Show("Selecciona Hub.")
            Exit Sub
        End If
        If cbPoste.Text.Length = 0 Then
            MessageBox.Show("Selecciona Poste.")
            Exit Sub
        End If
        eOpVentas = 102
        eClv_Sector = cbSector.SelectedValue
        eIdPoste = cbPoste.SelectedValue
        Dim FrmImpComi As New FrmImprimirComision
        FrmImpComi.ShowDialog()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        eOpVentas = 0
        eClv_Sector = 0
        eIdPoste = 0
        Me.Close()
    End Sub

    Private Sub FrmRepClientesClaveTecnica_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        eOpVentas = 0
        eClv_Sector = 0
        eIdPoste = 0
    End Sub

    Private Sub FrmRepClientesClaveTecnica_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        eClv_Sector = 0
        eIdPoste = 0
        MUESTRASectorReporte()
        MUESTRAPosteReporte()

    End Sub
End Class