Imports System.Data.SqlClient

Public Class FrmSelCiudad

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim x As Integer = 0
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'If x > 0 Then
        '    For y = 0 To (x - 1)
        '        Me.ListBox2.SelectedIndex = y
        '        If (Me.ListBox1.Text = Me.ListBox2.Text) Then
        '            MsgBox("La Ciudad ya esta en la lista", MsgBoxStyle.Information)
        '            Exit Sub
        '            'Else
        '            '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '            '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '            '    Exit Sub
        '        End If
        '    Next
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'Else
        '    Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '    Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        'End If
        x = CInt(Me.ListBox1.SelectedValue)
        If x > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Connection = CON
            Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Fill(Me.DataSetLidia.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmp, LocClv_session, CInt(Me.ListBox1.SelectedValue))
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            CON.Close()
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox1.Items.Count()
        'If Me.ListBox2.Items.Count() > 0 Then
        '    MsgBox("Primero Borre la(s) Ciudad(es) Seleccionadas", MsgBoxStyle.Information)
        'Else
        '    Me.ListBox1.SelectedIndex = 0
        '    For y = 1 To x
        '        Me.ListBox1.SelectedIndex = (y - 1)
        '        Me.ListBox2.Items.Add(Me.ListBox1.Text)
        '        Me.ListBox3.Items.Add(Me.ListBox1.SelectedValue.ToString)
        '    Next
        'End If
        x = CInt(Me.ListBox1.SelectedValue)
        If x > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Connection = CON
            Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.Fill(Me.DataSetLidia.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmp, LocClv_session)
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim x As Integer
        'If (Me.ListBox2.SelectedIndex <> -1) Then
        '    Me.ListBox3.SelectedIndex = Me.ListBox2.SelectedIndex
        '    Me.ListBox2.Items.RemoveAt(Me.ListBox2.SelectedIndex)
        '    Me.ListBox3.Items.RemoveAt(Me.ListBox3.SelectedIndex)
        'Else
        '    MsgBox("Selecciona primero un valor a borrar")
        'End If
        x = CInt(Me.ListBox2.SelectedValue)
        If x > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Connection = CON
            Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Fill(Me.DataSetLidia.PONUNOSelecciona_CiudadTmp_Seleccion_Ciudad, LocClv_session, CInt(Me.ListBox2.SelectedValue))
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            CON.Close()
        End If

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim x As Integer
        'Dim y As Integer
        'x = Me.ListBox2.Items.Count()
        'For y = 0 To (x - 1)
        '    Me.ListBox2.Items.RemoveAt(0)
        '    Me.ListBox3.Items.RemoveAt(0)
        'Next
        x = CInt(Me.ListBox2.SelectedValue)
        If x > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Connection = CON
            Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.Fill(Me.DataSetLidia.PONTODOSSelecciona_CiudadTmp_Seleccion_Ciudad, LocClv_session)
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
            Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Connection = CON
            Me.MuestraSeleccion_CiudadCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSeleccion_CiudadCONSULTA, LocClv_session)
            CON.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Borra_Seleccion_CiudadTmpTableAdapter.Connection = CON
        Me.Borra_Seleccion_CiudadTmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Seleccion_CiudadTmp, LocClv_session)
        CON.Close()
        eBndDeco = False
        LEdo_Cuenta = False
        LEdo_Cuenta2 = False
        Me.Close()


    End Sub

    Private Sub FrmSelCiudad_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    End Sub


    Private Sub FrmSelCiudad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If rMensajes = True Then
            LocClv_session = rSession
        End If
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        '  Me.Muestra_ciudadTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_ciudad)
        Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.Connection = CON
        Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpNUEVO, LocClv_session)
        Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Connection = CON
        Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.Fill(Me.DataSetLidia.MuestraSelecciona_CiudadTmpCONSULTA, LocClv_session)
        CON.Close()
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim x As Integer = 0
        Dim y As Integer
        x = Me.ListBox2.Items.Count()
        If x = 0 Then
            MsgBox("Seleccione al Menos una ciudad", MsgBoxStyle.Information)
        Else
            'For y = 0 To (x - 1)
            '    Me.ListBox3.SelectedIndex = y
            '    Me.Inserta_Sel_ciudadTableAdapter.Fill(Me.ProcedimientosArnoldo2.inserta_Sel_ciudad, LocClv_session, CInt(Me.ListBox3.Text))
            'Next


            'If eBndDeco = True Then
            '    FrmSelCalles.Show()
            '    Me.Close()
            'End If

            If rMensajes = True Or eBndDeco = True Then
                FrmSelColonia_Rep21.Show()
            End If
            If LocbndPolizaCiudad = True Then
                FrmSelFechas.Show()
            Else
                FrmSelColonia_Rep21.Show()
            End If
            Me.Close()
        End If

    End Sub




End Class