Imports System.Data.SqlClient
Public Class FrmSelTipServRep

    Private Sub FrmSelTipServRep_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        
    End Sub

    Private Sub FrmSelTipServRep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_Seleccion_tipservppalTmpNuevoTableAdapter.Connection = CON
        Me.Muestra_Seleccion_tipservppalTmpNuevoTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Seleccion_tipservppalTmpNuevo, LocClv_session)
        Me.Muestra_Seleccion_tipservppalTmpConsultaTableAdapter.Connection = CON
        Me.Muestra_Seleccion_tipservppalTmpConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Seleccion_tipservppalTmpConsulta, LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Insertauno_Seleccion_clientesTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_clientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_clientes, LocClv_session, Me.ListBox1.SelectedValue)
        Muestra(LocClv_session)
        CON.Close()
    End Sub
    Private Sub Muestra(ByVal clv_session As Long)
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.Muestra_Seleccion_tipservppalConsultaTableAdapter.Connection = CON2
        Me.Muestra_Seleccion_tipservppalConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Seleccion_tipservppalConsulta, clv_session)
        Me.Muestra_Seleccion_tipservppalTmpConsultaTableAdapter.Connection = CON2
        Me.Muestra_Seleccion_tipservppalTmpConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Seleccion_tipservppalTmpConsulta, clv_session)
        CON2.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Insertauno_Seleccion_clientestmpTableAdapter.Connection = CON
        Me.Insertauno_Seleccion_clientestmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_clientestmp, LocClv_session, Me.ListBox2.SelectedValue)
        Muestra(LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTodos_Seleccion_clientesTableAdapter.Connection = CON
        Me.InsertaTodos_Seleccion_clientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTodos_Seleccion_clientes, LocClv_session)
        Muestra(LocClv_session)
        CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTodos_Seleccion_clientestmpTableAdapter.Connection = CON
        Me.InsertaTodos_Seleccion_clientestmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTodos_Seleccion_clientestmp, LocClv_session)
        CON.Close()
        Muestra(LocClv_session)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If LocOp = 80 Then
            Borra_Tablas_Reporte_mensualidades(LocClv_session)
        End If
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim a As Integer = Nothing
        a = Me.ListBox2.Items.Count()
        If a = 0 Then
            MsgBox("Seleccione Al Menos Un Tipo De Servicio", MsgBoxStyle.Information)
            Exit Sub
        End If
        If LocOp = 30 Or LocOp = 80 Or eOpVentas = 80 Then
            My.Forms.FrmSelServRep.Show()
            Me.Close()

        End If
    End Sub

End Class
