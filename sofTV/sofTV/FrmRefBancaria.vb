Imports System.Data.SqlClient
Public Class FrmRefBancaria


    Private Sub FrmRefBancaria_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

        Dim con As New SqlClient.SqlConnection(MiConexion)
        Try
            con.Open()
            Me.Muestra_Bancos1TableAdapter.Connection = con
            Me.Muestra_Bancos1TableAdapter.Fill(Me.DataSetyahve.Muestra_Bancos1)
            Me.Muestra_Nombre_ClienteTableAdapter.Connection = con
            Me.Muestra_Nombre_ClienteTableAdapter.Fill(Me.DataSetyahve.Muestra_Nombre_Cliente)
            con.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        colorea(Me, Me.Name)

        If opcion = "N" Then
            Me.BindingNavigatorSaveItem.Enabled = True
            Me.TextBox1.ReadOnly = True
        ElseIf opcion = "C" Then
            Me.BindingNavigatorSaveItem.Enabled = False
            Me.BindingNavigatorDeleteItem.Enabled = False
            Me.TextBox1.ReadOnly = True
            Me.ComboBox2.Enabled = False
            Me.ComboBox1.Enabled = False
            Me.TextBox2.ReadOnly = True
            Consulta()
        ElseIf opcion = "M" Then
            Me.BindingNavigatorSaveItem.Enabled = True
            Me.BindingNavigatorDeleteItem.Enabled = True
            Me.TextBox1.ReadOnly = True
            Me.ComboBox1.Enabled = False
            Me.ComboBox2.Enabled = False
            Consulta()
        End If
        sw_refbanc = True
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorSaveItem.Click
        Verifica()
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub Verifica()
        If Me.ComboBox2.SelectedValue = 0 Then
            MsgBox("Seleccione el Cliente", MsgBoxStyle.Information, "Mensaje")
            Exit Sub
        ElseIf Me.ComboBox1.SelectedValue = 0 Then
            MsgBox("Seleccione el Banco", MsgBoxStyle.Information, "Mensaje")
            Exit Sub
        ElseIf Me.TextBox2.Text = "" Then
            MsgBox("Ingrese la Referencia", MsgBoxStyle.Information, "Mensaje")
            Exit Sub
        End If
        Guarda()
    End Sub

    Private Sub Guarda()

        If opcion = "N" Then
            nuevo()
        ElseIf opcion = "M" Then
            Modificacion()
        End If

    End Sub

    Private Sub Consulta()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("CONSULTA_RefBancaria", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Contrato_RefBancaria", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = Contrato_RefBancaria
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "Banco_RefBancaria", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = Banco_RefBancaria
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()
                Me.TextBox1.Text = reader(0).ToString
                Me.ComboBox2.SelectedValue = CInt(reader(0).ToString)
                Me.ComboBox1.SelectedValue = CInt(reader(1).ToString)
                Me.TextBox2.Text = reader(2).ToString
            End While

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        reader.Close()
        con.Close()


    End Sub

    Private Sub nuevo()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("NUEVO_RefBancaria", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Contrato", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.TextBox1.Text)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Banco", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox1.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                "@Referencia", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.TextBox2.Text
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            MsgBox("Se Guardo con �xito", MsgBoxStyle.Information, "Mensaje")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()

    End Sub

    Private Sub Modificacion()

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MODIFICA_RefBancaria", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Contrato", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.TextBox1.Text)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Banco", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = CInt(Me.ComboBox1.SelectedValue)
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                "@Referencia", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.TextBox2.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Contrato_RefBancaria", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = Contrato_RefBancaria
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Banco_RefBancaria", SqlDbType.Int)
        prm.Direction = ParameterDirection.Input
        prm.Value = Banco_RefBancaria
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            MsgBox("Se Actualizo con �xito", MsgBoxStyle.Information, "Mensaje")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.TextBox1.Text = Me.ComboBox2.SelectedValue
    End Sub
End Class