<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMotCan
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataSetEric = New sofTV.DataSetEric
        Me.ConMotCanBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConMotCanTableAdapter = New sofTV.DataSetEricTableAdapters.ConMotCanTableAdapter
        Me.MOTCANComboBox = New System.Windows.Forms.ComboBox
        Me.Clv_MOTCANTextBox = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConMotCanBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConMotCanBindingSource
        '
        Me.ConMotCanBindingSource.DataMember = "ConMotCan"
        Me.ConMotCanBindingSource.DataSource = Me.DataSetEric
        '
        'ConMotCanTableAdapter
        '
        Me.ConMotCanTableAdapter.ClearBeforeFill = True
        '
        'MOTCANComboBox
        '
        Me.MOTCANComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMotCanBindingSource, "MOTCAN", True))
        Me.MOTCANComboBox.DataSource = Me.ConMotCanBindingSource
        Me.MOTCANComboBox.DisplayMember = "MOTCAN"
        Me.MOTCANComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MOTCANComboBox.FormattingEnabled = True
        Me.MOTCANComboBox.Location = New System.Drawing.Point(49, 49)
        Me.MOTCANComboBox.Name = "MOTCANComboBox"
        Me.MOTCANComboBox.Size = New System.Drawing.Size(432, 24)
        Me.MOTCANComboBox.TabIndex = 2
        Me.MOTCANComboBox.ValueMember = "Clv_MOTCAN"
        '
        'Clv_MOTCANTextBox
        '
        Me.Clv_MOTCANTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConMotCanBindingSource, "Clv_MOTCAN", True))
        Me.Clv_MOTCANTextBox.Location = New System.Drawing.Point(410, 120)
        Me.Clv_MOTCANTextBox.Name = "Clv_MOTCANTextBox"
        Me.Clv_MOTCANTextBox.ReadOnly = True
        Me.Clv_MOTCANTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_MOTCANTextBox.TabIndex = 3
        Me.Clv_MOTCANTextBox.TabStop = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(345, 104)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(45, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(378, 24)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Selecciona el Motivo de la Cancelación"
        '
        'FrmMotCan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(536, 161)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Clv_MOTCANTextBox)
        Me.Controls.Add(Me.MOTCANComboBox)
        Me.Name = "FrmMotCan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Motivo de Cancelación"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConMotCanBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConMotCanBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConMotCanTableAdapter As sofTV.DataSetEricTableAdapters.ConMotCanTableAdapter
    Friend WithEvents MOTCANComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_MOTCANTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
