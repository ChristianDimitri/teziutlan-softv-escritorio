﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTrabajosTecnico
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.CMBNombre = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New Softv.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CMBQ = New System.Windows.Forms.Label()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.CMBO = New System.Windows.Forms.Label()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Cita = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Queja_o_orden_o_Otro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clave_Orden_Queja = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fec_Sol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hora = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Turno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRICPION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Obs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONTQUEJAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONTORDENES = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTALQUEJAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTALORDENES = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(714, 520)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(159, 36)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "&CERRAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Clv_Cita, Me.Contrato, Me.Queja_o_orden_o_Otro, Me.Clave_Orden_Queja, Me.Estatus, Me.Fec_Sol, Me.hora, Me.Turno, Me.DESCRICPION, Me.Obs, Me.CONTQUEJAS, Me.CONTORDENES, Me.TOTALQUEJAS, Me.TOTALORDENES})
        Me.DataGridView1.Location = New System.Drawing.Point(17, 96)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(868, 401)
        Me.DataGridView1.TabIndex = 5
        '
        'CMBNombre
        '
        Me.CMBNombre.AutoSize = True
        Me.CMBNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNombre.Location = New System.Drawing.Point(178, 33)
        Me.CMBNombre.Name = "CMBNombre"
        Me.CMBNombre.Size = New System.Drawing.Size(63, 20)
        Me.CMBNombre.TabIndex = 6
        Me.CMBNombre.Text = "Label1"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(14, 33)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(127, 16)
        Me.CMBLabel1.TabIndex = 7
        Me.CMBLabel1.Text = "Nombre Tecnico:"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(638, 28)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(219, 21)
        Me.DateTimePicker1.TabIndex = 8
        Me.DateTimePicker1.Value = New Date(2007, 5, 3, 0, 0, 0, 0)
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CMBLabel5.Location = New System.Drawing.Point(143, 532)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(150, 16)
        Me.CMBLabel5.TabIndex = 9
        Me.CMBLabel5.Text = "TOTAL DE QUEJAS:"
        '
        'CMBQ
        '
        Me.CMBQ.AutoSize = True
        Me.CMBQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBQ.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CMBQ.Location = New System.Drawing.Point(302, 529)
        Me.CMBQ.Name = "CMBQ"
        Me.CMBQ.Size = New System.Drawing.Size(0, 16)
        Me.CMBQ.TabIndex = 10
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CMBLabel7.Location = New System.Drawing.Point(388, 532)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(164, 16)
        Me.CMBLabel7.TabIndex = 11
        Me.CMBLabel7.Text = "TOTAL DE ORDENES:"
        '
        'CMBO
        '
        Me.CMBO.AutoSize = True
        Me.CMBO.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBO.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CMBO.Location = New System.Drawing.Point(560, 529)
        Me.CMBO.Name = "CMBO"
        Me.CMBO.Size = New System.Drawing.Size(0, 16)
        Me.CMBO.TabIndex = 12
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.Visible = False
        '
        'Clv_Cita
        '
        Me.Clv_Cita.DataPropertyName = "Clv_Cita"
        Me.Clv_Cita.HeaderText = "#Cita"
        Me.Clv_Cita.Name = "Clv_Cita"
        Me.Clv_Cita.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.Visible = False
        '
        'Queja_o_orden_o_Otro
        '
        Me.Queja_o_orden_o_Otro.DataPropertyName = "Queja_o_orden_o_Otro"
        Me.Queja_o_orden_o_Otro.HeaderText = "Tipo"
        Me.Queja_o_orden_o_Otro.Name = "Queja_o_orden_o_Otro"
        '
        'Clave_Orden_Queja
        '
        Me.Clave_Orden_Queja.DataPropertyName = "Clave_Orden_Queja"
        Me.Clave_Orden_Queja.HeaderText = "Clave_Orden_Queja"
        Me.Clave_Orden_Queja.Name = "Clave_Orden_Queja"
        '
        'Estatus
        '
        Me.Estatus.DataPropertyName = "Estatus"
        Me.Estatus.HeaderText = "Estatus"
        Me.Estatus.Name = "Estatus"
        '
        'Fec_Sol
        '
        Me.Fec_Sol.DataPropertyName = "Fec_Sol"
        Me.Fec_Sol.HeaderText = "Fecha Solicitud"
        Me.Fec_Sol.Name = "Fec_Sol"
        '
        'hora
        '
        Me.hora.DataPropertyName = "hora"
        Me.hora.HeaderText = "Hora"
        Me.hora.Name = "hora"
        Me.hora.Visible = False
        '
        'Turno
        '
        Me.Turno.DataPropertyName = "Turno"
        Me.Turno.HeaderText = "Turno"
        Me.Turno.Name = "Turno"
        '
        'DESCRICPION
        '
        Me.DESCRICPION.DataPropertyName = "DESCRICPION"
        Me.DESCRICPION.HeaderText = "Descripcion"
        Me.DESCRICPION.Name = "DESCRICPION"
        '
        'Obs
        '
        Me.Obs.DataPropertyName = "Obs"
        Me.Obs.HeaderText = "Observaciones"
        Me.Obs.Name = "Obs"
        '
        'CONTQUEJAS
        '
        Me.CONTQUEJAS.DataPropertyName = "CONTQUEJAS"
        Me.CONTQUEJAS.HeaderText = "Numero de Quejas"
        Me.CONTQUEJAS.Name = "CONTQUEJAS"
        Me.CONTQUEJAS.Visible = False
        '
        'CONTORDENES
        '
        Me.CONTORDENES.DataPropertyName = "CONTORDENES"
        Me.CONTORDENES.HeaderText = "Numero de ordenes"
        Me.CONTORDENES.Name = "CONTORDENES"
        Me.CONTORDENES.Visible = False
        '
        'TOTALQUEJAS
        '
        Me.TOTALQUEJAS.DataPropertyName = "TOTALQUEJAS"
        Me.TOTALQUEJAS.HeaderText = "Total de Quejas"
        Me.TOTALQUEJAS.Name = "TOTALQUEJAS"
        Me.TOTALQUEJAS.Visible = False
        '
        'TOTALORDENES
        '
        Me.TOTALORDENES.DataPropertyName = "TOTALORDENES"
        Me.TOTALORDENES.HeaderText = "Total de ordenes"
        Me.TOTALORDENES.Name = "TOTALORDENES"
        Me.TOTALORDENES.Visible = False
        '
        'FrmTrabajosTecnico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(899, 568)
        Me.Controls.Add(Me.CMBO)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Controls.Add(Me.CMBQ)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.CMBNombre)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Button3)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FrmTrabajosTecnico"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Trabajos Asignados Tecnico"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents CMBNombre As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CMBQ As System.Windows.Forms.Label
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents CMBO As System.Windows.Forms.Label
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Cita As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Queja_o_orden_o_Otro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clave_Orden_Queja As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fec_Sol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hora As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Turno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRICPION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Obs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONTQUEJAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONTORDENES As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTALQUEJAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTALORDENES As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
